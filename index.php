<?php
  include 'koneksi.php';
  session_start();

  if (!isset($_SESSION['login'])) {
    header('location:login.php');
  }else{
    $level=$_SESSION['login'];
    if ($level==4) {
      header('location:transaction.php');
    }
  }
 ?>
