<?php
include 'koneksi.php';
session_start();
if (!isset($_SESSION['login'])) {
  header('location:index.php');
}else{
  $nama=$_SESSION['name'];
  $username=$_SESSION['username'];
  $id_user=$_SESSION['id_user'];
}
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Transaksi Harian Bengkel</title>
    <link rel="stylesheet" href="asset/materialize.min.css">
    <link rel="stylesheet" href="asset/style.css">

  </head>
  <body>
    <nav>
      <ul class="container">
        <li>Transaksi Harian Bengkel - <?php echo $username; ?> </li>
        <li class="right"><a href="logout.php">Keluar</a></li>
      </ul>
    </nav>
    <div class="container">
      <br>
      <a href="add_transaction.php" class="btn blue">Tambah Transaksi</a>
      <div class="right">
      Total Transaksi Hari ini<h6>Rp.12.300.000</h6>
      </div>
      <br>
      <table>
      <thead>
        <tr>
            <th>No</th>
            <th>Nama Pelanggan</th>
            <th>Tipe Transaksi</th>
            <th>Harga</th>
            <th>Dilayani Oleh</th>
            <th></th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>1</td>
          <td>Fahrezi</td>
          <td>Ganti Oli</td>
          <td>Rp.300.000</td>
          <td>Genta</td>
          <td>
            <a class='dropdown-trigger btn z-depth-0 white waves-effect wave-teal aa' href='#' data-target='dropdown1'>:</a>
              <ul id='dropdown1' class='dropdown-content'>
                <li><a href="#!">Edit</a></li>
                <li><a href="#!">Hapus</a></li>
              </ul>
          </td>
        </tr>

      </tbody>
    </table>
    </div>


<script src="asset/jquery.js" charset="utf-8"></script>
<script src="asset/materialize.min.js" charset="utf-8"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      $('.dropdown-trigger').dropdown();

    });

  // Or with jQuery


    </script>
    </body>
</html>
